package com.korytnyi.testtask;

import com.korytnyi.testtask.counter.CodeLinesCounter;
import com.korytnyi.testtask.counter.LinesCounter;
import com.korytnyi.testtask.output.DisplayOutput;
import com.korytnyi.testtask.output.ResultOutput;
import com.korytnyi.testtask.reader.FileReader;
import com.korytnyi.testtask.reader.FileReaderNio;
import com.korytnyi.testtask.text.ResultFormatter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Java console application that implements Counting Code Lines
 */
public class App {

    public static void main(String[] args) {
        // print intro
        System.out.println("******** Welcome to the Code Lines Counting Application! ********");
        // print usage info if parameter omitted
        if (args.length == 0) {
            System.out.println("Usage: java -jar lines-counter-1.0.jar [file|folder]");
        } else {
            Path path = Paths.get(args[0]);
            FileReader reader = new FileReaderNio();
            LinesCounter counter = new CodeLinesCounter();
            ResultOutput output = new DisplayOutput();

            // count code lines for provided path
            Map<Path, Long> countResult = new LinkedHashMap<>();
            try {
                Map<Path, Long> result = doCounting(path, reader, counter);
                countResult.putAll(result);
            } catch (IOException e) {
                System.out.println("Can't read file: " + e.getMessage());
            } catch (Exception e) {
                System.out.println("Unexpected error: " + e.getMessage());
            }
            // format result data for output
            List<String> formattedLines = ResultFormatter.format(countResult);
            output.lines(formattedLines);
        }
    }

    /**
     * Counts lines of java code for specified path
     *
     * @param path    a starting file can be file|folder
     * @param reader  an file reader
     * @param counter an lines counter
     * @return a Map where key is file|folder and value is number of code lines inside
     */
    static Map<Path, Long> doCounting(Path path, FileReader reader, LinesCounter counter)
            throws IOException {
        Map<Path, Long> resultMap = new LinkedHashMap<>();
        List<Path> files = reader.readFiles(path);

        for (Path file : files) {
            long newLinesCount = 0;
            // folder initialization with the initial value
            if (Files.isDirectory(file)) {
                resultMap.put(file, newLinesCount);
            }
            // count lines only for java classes
            if (Files.isRegularFile(file) && file.toFile().getName().toLowerCase().endsWith(".java")) {
                List<String> lines = reader.readLines(file);
                newLinesCount = counter.count(lines);
                resultMap.put(file, newLinesCount);
            }
            // increase lines count for all parent folders
            if (newLinesCount != 0 && Files.isDirectory(path)) {
                Path parentFolder = file;
                do {
                    // move to level up on folder tree
                    parentFolder = parentFolder.getParent();

                    if (!resultMap.containsKey(parentFolder)) {
                        resultMap.put(parentFolder, newLinesCount);
                    } else {
                        // increase lines count if folder exists
                        long existingLinesCount = resultMap.get(parentFolder);
                        resultMap.put(parentFolder, existingLinesCount + newLinesCount);
                    }
                } while (!parentFolder.equals(path));
            }
        }

        // filter folders with no lines of code inside
        return resultMap.entrySet()
                .stream()
                .filter(e -> e.getValue() != 0)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (x, y) -> y, LinkedHashMap::new));
    }
}
