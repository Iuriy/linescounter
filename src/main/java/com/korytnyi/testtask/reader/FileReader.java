package com.korytnyi.testtask.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public interface FileReader {
    List<String> readLines(Path file) throws IOException;

    List<Path> readFiles(Path path) throws IOException;
}
