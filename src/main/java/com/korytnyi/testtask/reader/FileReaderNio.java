package com.korytnyi.testtask.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of {@link FileReader} reads data from a file with using of Java NIO
 */
public class FileReaderNio implements FileReader {
    /**
     * Reads all lines from file
     *
     * @param file a file to read data
     * @return all lines of the file
     * @throws IOException if an I/O error is thrown while reading data from the file
     */
    @Override
    public List<String> readLines(Path file) throws IOException {
        return Files.readAllLines(file);
    }

    /**
     * Return a List of files by walking the file tree rooted at a given starting file.
     *
     * @param path the starting file
     * @return a List of files
     * @throws IOException if an I/O error is thrown while reading the file
     */
    @Override
    public List<Path> readFiles(Path path) throws IOException {
        return Files.walk(path).collect(Collectors.toList());
    }
}
