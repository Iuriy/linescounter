package com.korytnyi.testtask.text;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.korytnyi.testtask.text.Format.SEMICOLON_SEPARATOR;

/**
 * Used for data formatting
 */
public class ResultFormatter {

    /**
     * Formats data in default format
     *
     * @param linesInfo a map where key is file name and value is number of lines
     * @return a List of formatted String
     */
    public static List<String> format(Map<Path, Long> linesInfo) {
        // return result in default format
        return format(linesInfo, SEMICOLON_SEPARATOR);
    }

    /**
     * Formats data in special format
     *
     * @param linesInfo    a map where key is file name and value is number of lines
     * @param resultFormat a format string
     * @return a List of formatted String
     */
    public static List<String> format(Map<Path, Long> linesInfo, Format resultFormat) {
        List<String> formattedData = new ArrayList<>();
        String format = resultFormat.toString();
        // proceed only valid data
        if (linesInfo != null && !linesInfo.isEmpty()) {
            for (Path key : linesInfo.keySet()) {
                String indentSpaces = getIndentSpaces(key);
                String formattedLine = String.format(indentSpaces + format, key.getFileName(), linesInfo.get(key));
                formattedData.add(formattedLine);
            }
        }
        return formattedData;
    }

    private static String getIndentSpaces(Path file) {
        String path = file.toString();

        // count file separators in the path
        int indent = path.length() - path.replace(File.separator, "").length();
        // create string with "indent" spaces
        String indentSpaces = (indent == 0) ? "" : String.format("%" + indent + "s", "");

        return indentSpaces;
    }
}
