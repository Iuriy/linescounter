package com.korytnyi.testtask.text;

public enum Format {
    SEMICOLON_SEPARATOR("%s : %d");

    private final String text;

    Format(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
