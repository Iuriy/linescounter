package com.korytnyi.testtask.counter;

import java.util.List;

public interface LinesCounter {
    long count(List<String> lines);
}
