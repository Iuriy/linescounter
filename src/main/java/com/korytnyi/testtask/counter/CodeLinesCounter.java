package com.korytnyi.testtask.counter;

import java.util.List;

/**
 * Implementation of {@link LinesCounter} for counting actual lines of code
 */
public class CodeLinesCounter implements LinesCounter {
    /**
     * Counts the number of lines of actual code
     *
     * @param lines contains data from file
     * @return a number of lines of actual code
     */
    @Override
    public long count(List<String> lines) {
        long result = 0;

        if (lines != null) {
            result = lines.stream().filter(this::isActualCode).count();
        }

        return result;
    }

    private boolean isActualCode(String line) {
        // case 1 - empty string i.e contains only tabs, spaces, carriage returns
        if (line.trim().length() == 0) {
            return false;
        }
        // case 2 - java comments
        if (removeJavaComments(line).length() == 0) {
            return false;
        }

        return true;
    }

    private String removeJavaComments(String line) {
        return line.replaceAll("\\s*//.*", "") // single line comment
                .replaceAll("/\\*.*?\\*/", "") // block comments
                .replaceAll("/\\*.*", "") // multiline block comment start i.e. "/*"
                .replaceAll(".*\\*/", "") // multiline block comment end i.e. "*/"
                .replaceAll("\\s*\\*.*", "") // javadoc block comment middle i.e. " * "
                .trim();
    }
}
