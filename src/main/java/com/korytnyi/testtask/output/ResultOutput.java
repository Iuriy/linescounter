package com.korytnyi.testtask.output;

import java.util.List;

public interface ResultOutput {
    void lines(List<String> output);
}
