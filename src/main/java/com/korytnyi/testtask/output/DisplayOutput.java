package com.korytnyi.testtask.output;

import java.util.List;

/**
 * Implementation of {@link ResultOutput} for displaying the result
 */
public class DisplayOutput implements ResultOutput {
    /**
     * Displays result on the screen
     *
     * @param output a List of data to displaying
     */
    public void lines(List<String> output) {
        if (output != null && output.size() > 0) {
            for (String string : output) {
                System.out.println(string);
            }
        }
    }
}
