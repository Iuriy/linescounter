package com.korytnyi.testtask;

import com.korytnyi.testtask.counter.CodeLinesCounter;
import com.korytnyi.testtask.counter.LinesCounter;
import com.korytnyi.testtask.reader.FileReader;
import com.korytnyi.testtask.reader.FileReaderNio;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class AppTest {

    private FileReader reader = null;
    private LinesCounter counter = null;

    @Before
    public void setUp() {
        reader = new FileReaderNio();
        counter = new CodeLinesCounter();
    }

    @Test
    public void doCounting_SampleTestFileOne_ResultMap() throws Exception {
        Path file = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource("test-files/File3LinesOfCode.java")).toURI());

        Map<Path, Long> resultMap = App.doCounting(file, reader, counter);

        assertEquals(1, resultMap.size());
        assertEquals(3, resultMap.get(file).longValue());
    }

    @Test
    public void count_SampleTestFileTwo_ResultMap() throws Exception {
        Path file = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource("test-files/File5LinesOfCode.java")).toURI());

        Map<Path, Long> resultMap = App.doCounting(file, reader, counter);

        assertEquals(1, resultMap.size());
        assertEquals(5, resultMap.get(file).longValue());
    }

    @Test
    public void count_SampleFolder_ResultMap() throws Exception {
        Path file = Paths.get(Objects.requireNonNull(getClass()
                .getClassLoader().getResource("test-files")).toURI());

        Map<Path, Long> resultMap = App.doCounting(file, reader, counter);

        assertEquals(3, resultMap.size());
        assertEquals(8, resultMap.get(file).longValue());
    }

    @Test()
    public void main_ExistingFileAsParameter_NoException() throws Exception {
        Path file = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource("test-files/File3LinesOfCode.java")).toURI());
        String[] args = {file.toString()};

        App.main(args);
    }

    @Test()
    public void main_ExistingFolderAsParameter_NoException() throws Exception {
        Path path = Paths.get(Objects.requireNonNull(getClass()
                .getClassLoader().getResource("test-files")).toURI());
        String[] args = {path.toString()};

        App.main(args);
    }

    @Test()
    public void main_NotExistingFileAsParameter_NoException() {
        String[] args = {"Qw32Eds.java"};

        App.main(args);
    }
}
