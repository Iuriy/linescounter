package com.korytnyi.testtask.counter;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CodeLinesCounterTest {

    private LinesCounter counter = null;

    @Before
    public void setUp() {
        counter = new CodeLinesCounter();
    }

    @Test
    public void count_Null_ResultZero() {
        long linesOfCode = counter.count(null);
        assertEquals(0, linesOfCode);
    }

    @Test
    public void count_EmptyList_ResultZero() {
        long linesOfCode = counter.count(new ArrayList<>());
        assertEquals(0, linesOfCode);
    }

    @Test
    public void count_WhitespaceCharacters_ResultZero() {
        List<String> lines = Arrays.asList("\t", "\r", "\n", "\f", "\b", " ");

        long linesOfCode = counter.count(lines);
        assertEquals(0, linesOfCode);
    }

    @Test
    public void count_JavaComments_ResultZero() {
        List<String> lines = Arrays.asList(
                " // single line comment ", "//",
                " /* block */ /* comments */ ", "/**/",
                " /* multiple-line block comment start", "/*",
                " multiple-line block comment end */ ", "*/",
                " /* mixed */ // comments", "/**/ //",
                " /** java doc start ", "/**",
                " * java doc middle ", "*",
                " * java doc end */ ", "* */"
        );

        long linesOfCode = counter.count(lines);
        assertEquals(0, linesOfCode);
    }

    @Test
    public void count_JavaCodeMixedWithComments_AllStringsAreValid() {
        List<String> lines = Arrays.asList(
                " /* block */ int i;/* comments */ ", "/**/int i;",
                " int i;/* multiple-line block comment start", "int i;/*",
                " multiple-line block comment end */ int i;", "*/int i;",
                " /* mixed */ int i; // comments", "/**/int i;//",
                " import java.util.List;/** java doc start ", "import java.util.List;/**",
                " * java doc end */ int i; ", "* */int i;"
        );

        long linesOfCode = counter.count(lines);
        assertEquals(lines.size(), linesOfCode);
    }

    @Test
    public void count_PureJavaCode_AllStringsAreValid() {
        List<String> lines = Arrays.asList(
                " int i; ",
                " import java.util.List \n",
                "\t return result;",
                " System.out.println(\"Ok\")",
                " public class AppTest {"
        );

        long linesOfCode = counter.count(lines);
        assertEquals(lines.size(), linesOfCode);
    }
}