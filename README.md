# LinesCounter

### Description
 The application counts the number of lines of actual code in a Java source file,
i.e. empty lines that contains only whitespace characters and comment lines not be counted.

### Build & Run
 Please use **run.sh** script to build and execute application.

### Manual Run
```sh
 $ java -jar lines-counter-1.0.jar [file|folder]
 ```
